/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.system;

import java.util.Iterator;

import com.blang.artie.entity.Domain;
import com.blang.artie.entity.Partition;
import com.blang.artie.entity.World;

// TODO: Auto-generated Javadoc
public abstract class IterativeSystem extends BaseSystem {

	/**
	 * Instantiates a new iterative system.
	 *
	 * @param world the world
	 * @param domainId the domain id
	 */
	public IterativeSystem(World world, int domainId) {
		super(world, domainId);
	}

	/**
	 * Process.
	 *
	 * @param partition the partition
	 * @param timestamp the timestamp
	 */
	public void process(Partition partition, long timestamp) {
		Domain domain = partition.getDomain(domainId);

		Iterator<Integer> iterator = domain.iterator();

		while (iterator.hasNext()) {
			int index = iterator.next();
			processMe(partition, index);
		}
		lastUpdate = timestamp;
	}

	/**
	 * Iterate me.
	 *
	 * @param partition the partition
	 * @param index            the index
	 */
	protected abstract void processMe(Partition partition, int index);

}
