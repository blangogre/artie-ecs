/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.system;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

import com.blang.artie.entity.EntityEvent;
import com.blang.artie.entity.Partition;
import com.blang.artie.entity.World;
import com.blang.artie.entity.Domain;

// TODO: Auto-generated Javadoc
public abstract class QueueSystem<T extends EntityEvent> extends BaseSystem {

	protected Deque<T> queue;

	/**
	 * Instantiates a new queue system.
	 *
	 * @param world the world
	 * @param domainId the domain id
	 */
	public QueueSystem(World world, int domainId) {
		super(world, domainId);
		queue = new ArrayDeque<>();
	}

	/**
	 * Poll.
	 *
	 * @param timestamp the timestamp
	 */
	public void poll(long timestamp) {
		Partition partition;
		Domain domain;

		Iterator<T> iterator = queue.iterator();
		while (iterator.hasNext()) {
			T event = iterator.next();
			int index = event.getIndex();

			partition = event.getPartition();
			domain = partition.getDomain(domainId);

			if (domain.contains(index)) {
				pollMe(partition, event);
			}
		}
		lastUpdate = timestamp;
	}

	/**
	 * Poll me.
	 *
	 * @param partition the partition
	 * @param event            the event
	 */
	protected abstract void pollMe(Partition partition, T event);

}
