/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.entity;

import java.util.HashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc
public class Partition {

	private final Set<EntityListener> listeners;

	private final Entity[] entities;
	private final ComponentManager componentManager;
	private final Domain[] domains;
	private final World world;
	private final int size;

	/**
	 * Instantiates a new entity partition.
	 *
	 * @param registry
	 *            the registry
	 * @param aspects
	 *            the aspects
	 * @param world
	 *            the world
	 * @param size
	 *            the size
	 */
	protected Partition(ComponentRegistry registry, Aspect[] aspects, World world, int size) {
		listeners = new HashSet<>();
		entities = new Entity[size];
		componentManager = new ComponentManager(registry, size);
		this.world = world;
		this.size = size;

		domains = createDomains(aspects);
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Creates the domains.
	 *
	 * @param aspects
	 *            the aspects
	 * @return the domain[]
	 */
	protected Domain[] createDomains(Aspect[] aspects) {
		int size = aspects.length;
		Domain[] domains = new Domain[size];

		for (int i = 0; i < size; i++) {
			Domain domain = new Domain(aspects[i], this);
			domains[i] = domain;
			addListener(domain);
		}

		return domains;
	}

	/**
	 * Gets the domain.
	 *
	 * @param domainId
	 *            the domain id
	 * @return the domain
	 */
	public Domain getDomain(int domainId) {
		return domains[domainId];
	}

	/**
	 * Adds the entity.
	 *
	 * @param entity
	 *            the entity
	 * @return the int
	 * @throws PartitionFullException
	 *             the partition full exception
	 */
	protected int addEntity(Entity entity) throws PartitionFullException {
		int length = entities.length;
		for (int index = 0; index < length; index++) {
			if (entities[index] == null) {
				entities[index] = entity;

				world.registerEntity(entity, this);

				for (EntityListener listener : listeners) {
					listener.entityAdded(this, index);
				}

				return index;
			}
		}
		throw new PartitionFullException("Entity creation failed.  EntityPartition is full.");
	}

	/**
	 * Gets the entity.
	 *
	 * @param index
	 *            the index
	 * @return the entity
	 */
	protected Entity getEntity(int index) {
		return entities[index];
	}

	/**
	 * Removes the entity.
	 *
	 * @param index
	 *            the index
	 * @return the entity
	 */
	protected Entity removeEntity(int index) {
		Entity entity = entities[index];

		entities[index] = null;
		componentManager.removeAll(index);
		world.unregisterEntity(entity, this);

		for (EntityListener listener : listeners) {
			listener.entityRemoved(this, index);
		}

		return entity;
	}

	/**
	 * Find entity.
	 *
	 * @param entity
	 *            the entity
	 * @return the int
	 * @throws EntityNotFoundException
	 *             the entity not found exception
	 */
	public int findEntity(Entity entity) throws EntityNotFoundException {
		for (int i = 0; i < size; i++) {
			if (entities[i].equals(entity)) {
				return i;
			}
		}
		throw new EntityNotFoundException("Entity: " + entity.getUuid() + " not found.");
	}

	/**
	 * Adds the component.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param index
	 *            the index
	 * @param component
	 *            the component
	 * @return the t
	 */
	protected <T extends Component> T addComponent(Class<T> clazz, int index, T component) {
		Entity entity = entities[index];
		entity.add(clazz);
		T copy = componentManager.addComponent(clazz, index, component);

		for (EntityListener listener : listeners) {
			listener.entityModified(this, index);
		}

		return copy;
	}

	/**
	 * Gets the component.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param index
	 *            the index
	 * @return the component
	 */
	protected <T extends Component> T getComponent(Class<T> clazz, int index) {
		return componentManager.getComponent(clazz, index);
	}

	/**
	 * Removes the component.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param index
	 *            the index
	 * @return the t
	 */
	protected <T extends Component> T removeComponent(Class<T> clazz, int index) {
		Entity entity = entities[index];
		entity.remove(clazz);
		T copy = componentManager.removeComponent(clazz, index);

		for (EntityListener listener : listeners) {
			listener.entityModified(this, index);
		}

		return copy;
	}

	/**
	 * Register system.
	 *
	 * @param listener
	 *            the system
	 * @return true, if successful
	 */
	public boolean addListener(EntityListener listener) {
		return listeners.add(listener);
	}

	/**
	 * Unregister system.
	 *
	 * @param listener
	 *            the system
	 * @return true, if successful
	 */
	public boolean removeListener(EntityListener listener) {
		return listeners.remove(listener);
	}

	/**
	 * Copy.
	 *
	 * @param clazz
	 *            the clazz
	 * @param indexSrc
	 *            the index from
	 * @param indexTgt
	 *            the index to
	 * @param partitionSrc
	 *            the partition from
	 * @param partitionTgt
	 *            the partition to
	 * @throws ComponentNotFoundException
	 */
	protected static void copy(Class<? extends Component> clazz, int indexSrc, int indexTgt, Partition partitionSrc,
			Partition partitionTgt) throws ComponentNotFoundException {
		ComponentManager managerSrc = partitionSrc.componentManager;
		ComponentManager managerTgt = partitionTgt.componentManager;

		if (managerSrc.contains(clazz) && managerTgt.contains(clazz)) {
			ComponentManager.copy(clazz, indexSrc, indexTgt, managerSrc, managerTgt);

			for (EntityListener listener : partitionTgt.listeners) {
				listener.entityModified(partitionTgt, indexTgt);
			}
		} else {
			throw new ComponentNotFoundException("Copy failed due to unknown Component type.");
		}
	}

}
