/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

// TODO: Auto-generated Javadoc
public class Entity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6302417282702527114L;

	private final UUID uuid;
	private final Set<Class<? extends Component>> mask;

	/**
	 * Instantiates a new entity.
	 */
	protected Entity() {
		this.uuid = UUID.randomUUID();
		this.mask = new HashSet<>();
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public UUID getUuid() {
		return uuid;
	}

	public boolean contains(Class<? extends Component> component) {
		return mask.contains(component);
	}

	public boolean containsAll(Collection<Class<? extends Component>> components) {
		return mask.containsAll(components);
	}

	public boolean disjoint(Collection<Class<? extends Component>> components) {
		return Collections.disjoint(mask, components);
	}

	public Iterator<Class<? extends Component>> iterator() {
		return mask.iterator();
	}

	protected boolean add(Class<? extends Component> component) {
		return mask.add(component);
	}

	protected boolean remove(Class<? extends Component> component) {
		return mask.remove(component);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(uuid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {

		if (o == this)
			return true;
		if (!(o instanceof Entity)) {
			return false;
		}
		Entity entity = (Entity) o;
		return uuid.equals(entity.uuid);
	}

}
