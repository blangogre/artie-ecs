/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.entity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

// TODO: Auto-generated Javadoc
public class ComponentManager {

	private final Map<Class<? extends Component>, Component[]> componentsMap;

	/**
	 * Instantiates a new component manager.
	 *
	 * @param registry
	 *            the registry
	 * @param size
	 *            the size
	 */
	protected ComponentManager(ComponentRegistry registry, int size) {
		this.componentsMap = new HashMap<>();
		Iterator<Class<? extends Component>> iterator = registry.iterator();

		while (iterator.hasNext()) {
			Class<? extends Component> clazz = iterator.next();
			componentsMap.put(clazz, new Component[size]);
		}
	}

	protected boolean contains(Class<? extends Component> clazz) {
		return componentsMap.containsKey(clazz);
	}

	/**
	 * Adds the component.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param index
	 *            the index
	 * @param component
	 *            the component
	 * @return the t
	 */
	protected <T extends Component> T addComponent(Class<T> clazz, int index, T component) {
		return clazz.cast(componentsMap.get(clazz)[index] = component);
	}

	/**
	 * Gets the component.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param index
	 *            the index
	 * @return the component
	 */
	protected <T extends Component> T getComponent(Class<T> clazz, int index) {
		return clazz.cast(componentsMap.get(clazz)[index]);
	}

	/**
	 * Removes the component.
	 *
	 * @param <T>
	 *            the generic type
	 * @param clazz
	 *            the clazz
	 * @param index
	 *            the index
	 * @return the t
	 */
	protected <T extends Component> T removeComponent(Class<T> clazz, int index) {
		T component = getComponent(clazz, index);
		componentsMap.get(clazz)[index] = null;
		return component;
	}

	/**
	 * Removes the all.
	 *
	 * @param index
	 *            the index
	 * @return the map< class<? extends component>, component>
	 */
	protected Map<Class<? extends Component>, Component> removeAll(int index) {
		Map<Class<? extends Component>, Component> map = new HashMap<>(componentsMap.size());

		for (Class<? extends Component> clazz : componentsMap.keySet()) {
			Component[] components = componentsMap.get(clazz);
			map.put(clazz, components[index]);
			componentsMap.get(clazz)[index] = null;
		}

		return map;
	}

	/**
	 * Copy.
	 *
	 * @param clazz
	 *            the clazz
	 * @param indexFrom
	 *            the index from
	 * @param indexTo
	 *            the index to
	 * @param managerFrom
	 *            the manager from
	 * @param managerTo
	 *            the manager to
	 */
	protected static void copy(Class<? extends Component> clazz, int indexFrom, int indexTo,
			ComponentManager managerFrom, ComponentManager managerTo) {
		Component from = managerFrom.componentsMap.get(clazz)[indexFrom];
		managerTo.componentsMap.get(clazz)[indexTo] = from;
	}

}
