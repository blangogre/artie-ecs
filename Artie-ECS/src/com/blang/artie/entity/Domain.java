/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.entity;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

// TODO: Auto-generated Javadoc
public class Domain implements EntityListener, Iterable<Integer> {

	private final Aspect aspect;
	private final Set<Integer> entitySet;

	/**
	 * Instantiates a new domain.
	 *
	 * @param aspect the aspect
	 * @param parent the parent
	 */
	protected Domain(Aspect aspect, Partition parent) {
		Comparator<Integer> comparator = aspect.getComparator();
		if (comparator != null) {
			entitySet = new TreeSet<>(comparator);
		} else {
			entitySet = new HashSet<>();
		}
		this.aspect = aspect;
	}

	/**
	 * Gets the aspect.
	 *
	 * @return the aspect
	 */
	public Aspect getAspect() {
		return aspect;
	}

	/**
	 * Contains.
	 *
	 * @param index the index
	 * @return true, if successful
	 */
	public boolean contains(int index) {
		return entitySet.add(index);
	}

	/* (non-Javadoc)
	 * @see com.blang.artie.entity.EntityListener#entityAdded(com.blang.artie.entity.Partition, int)
	 */
	@Override
	public void entityAdded(Partition partition, int index) {
	}

	/* (non-Javadoc)
	 * @see com.blang.artie.entity.EntityListener#entityModified(com.blang.artie.entity.Partition, int)
	 */
	@Override
	public void entityModified(Partition partition, int index) {
		Entity entity = partition.getEntity(index);
		if (aspect.interested(entity)) {
			entitySet.add(index);
		} else {
			entitySet.remove(index);
		}
	}

	/* (non-Javadoc)
	 * @see com.blang.artie.entity.EntityListener#entityRemoved(com.blang.artie.entity.Partition, int)
	 */
	@Override
	public void entityRemoved(Partition partition, int index) {
		entitySet.remove(index);
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Integer> iterator() {
		return entitySet.iterator();
	}

}
