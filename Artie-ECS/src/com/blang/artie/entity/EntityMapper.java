package com.blang.artie.entity;

public class EntityMapper {

	public int createEntity(Partition partition) throws PartitionFullException {
		return partition.addEntity(new Entity());
	}

	public Entity getEntity(Partition partition, int index) {
		return partition.getEntity(index);
	}

	public Entity removeEntity(Partition partition, int index) {
		return partition.removeEntity(index);
	}

}
