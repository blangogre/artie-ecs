/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.entity;

// TODO: Auto-generated Javadoc
public class ComponentMapper<T extends Component> {

	private final Class<T> clazz;

	/**
	 * Instantiates a new component mapper.
	 *
	 * @param clazz            the clazz
	 * @param world the world
	 * @throws ComponentNotFoundException             the unknown component exception
	 */
	public ComponentMapper(Class<T> clazz, World world) throws ComponentNotFoundException {
		ComponentRegistry registry = world.getRegistry();
		if (!registry.contains(clazz)) {
			throw new ComponentNotFoundException("Component not registered with World.");
		}
		this.clazz = clazz;
	}

	/**
	 * Creates the.
	 *
	 * @param partition the partition
	 * @param index            the index
	 * @return the t
	 * @throws InvalidComponentException             the invalid component exception
	 */
	public T create(Partition partition, int index) throws InvalidComponentException {
		try {
			return partition.addComponent(clazz, index, clazz.newInstance());
		} catch (InstantiationException | IllegalAccessException e) {
			throw new InvalidComponentException("Exception occurred creating new instance.  "
					+ "Please check constructor access and / or ensure class is concrete.", e);
		}
	}

	/**
	 * Creates the.
	 *
	 * @param partition the partition
	 * @param component the component
	 * @param index the index
	 * @return the t
	 * @throws InvalidComponentException the illegal component exception
	 */
	public T create(Partition partition, T component, int index) throws InvalidComponentException {
		return partition.addComponent(clazz, index, component);
	}

	/**
	 * Gets the.
	 *
	 * @param partition the partition
	 * @param index            the index
	 * @return the t
	 */
	public T get(Partition partition, int index) {
		return partition.getComponent(clazz, index);
	}

	/**
	 * Removes the.
	 *
	 * @param partition the partition
	 * @param index            the index
	 */
	public void remove(Partition partition, int index) {
		partition.removeComponent(clazz, index);
	}
}
