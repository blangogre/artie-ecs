/*
 * MIT License
 * 
 * Copyright (comparator) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.blang.artie.entity;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc
public class Aspect {

	private final Set<Class<? extends Component>> all;
	private final Set<Class<? extends Component>> one;
	private final Set<Class<? extends Component>> none;
	private final Comparator<Integer> comparator;

	/**
	 * Instantiates a new aspect.
	 *
	 * @param builder
	 *            the builder
	 */
	private Aspect(Builder builder) {
		all = builder.all;
		one = builder.one;
		none = builder.none;
		comparator = builder.comparator;
	}

	/**
	 * Interested.
	 *
	 * @param entity
	 *            the entity
	 * @return true, if successful
	 */
	public boolean interested(Entity entity) {

		return (all.isEmpty() || entity.containsAll(all)) && (one.isEmpty() || containsOne(entity))
				&& (none.isEmpty() || entity.disjoint(none));
	}

	/**
	 * Gets the comparator.
	 *
	 * @return the comparator
	 */
	protected Comparator<Integer> getComparator() {
		return comparator;
	}

	/**
	 * Contains one.
	 *
	 * @param setA
	 *            the set A
	 * @param setB
	 *            the set B
	 * @return true, if successful
	 */
	private boolean containsOne(Entity entity) {
		int count = 0;
		boolean result = false;
		for (Class<? extends Component> clazzB : one) {
			if (entity.contains(clazzB)) {
				if (count == 0) {
					result = true;
					count++;
					break;
				} else {
					return false;
				}
			}
		}
		return result;
	}

	public static class Builder {
		private Set<Class<? extends Component>> all;
		private Set<Class<? extends Component>> one;
		private Set<Class<? extends Component>> none;
		private Comparator<Integer> comparator;

		/**
		 * Instantiates a new builder.
		 *
		 * @param comparator
		 *            the comparator
		 */
		public Builder(Comparator<Integer> comparator) {
			all = new HashSet<>();
			one = new HashSet<>();
			none = new HashSet<>();
			this.comparator = comparator;
		}

		/**
		 * Instantiates a new builder.
		 */
		public Builder() {
			this(null);
		}

		/**
		 * All.
		 *
		 * @param clazzez
		 *            the clazzez
		 * @return the builder
		 */
		@SafeVarargs
		public final Builder includeAllOf(Class<? extends Component>... clazzez) {
			for (Class<? extends Component> clazz : clazzez) {
				all.add(clazz);
			}
			return this;
		}

		/**
		 * One.
		 *
		 * @param clazzez
		 *            the clazzez
		 * @return the builder
		 */
		@SafeVarargs
		public final Builder includeOneOf(Class<? extends Component>... clazzez) {
			for (Class<? extends Component> clazz : clazzez) {
				one.add(clazz);
			}
			return this;
		}

		/**
		 * None.
		 *
		 * @param clazzez
		 *            the clazzez
		 * @return the builder
		 */
		@SafeVarargs
		public final Builder includeNoneOf(Class<? extends Component>... clazzez) {
			for (Class<? extends Component> clazz : clazzez) {
				none.add(clazz);
			}
			return this;
		}

		/**
		 * Builds the.
		 *
		 * @return the aspect
		 */
		public Aspect build() {
			return new Aspect(this);
		}
	}
}
