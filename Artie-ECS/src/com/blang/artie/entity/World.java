/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.entity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

// TODO: Auto-generated Javadoc
public class World {

	private final Map<String, Partition> partitionMap;
	private final Map<Entity, Set<Partition>> entityMap;
	private final ComponentRegistry registry;
	private final Aspect[] aspects;

	/**
	 * Instantiates a new entity manager.
	 *
	 * @param builder
	 *            the builder
	 */
	private World(Builder builder) {
		this.partitionMap = new HashMap<>();
		this.entityMap = new HashMap<>();
		this.registry = builder.registry;
		this.aspects = builder.aspects;
	}

	/**
	 * Gets the registry.
	 *
	 * @return the registry
	 */
	public ComponentRegistry getRegistry() {
		return registry;
	}

	/**
	 * Creates the partition.
	 *
	 * @param key
	 *            the key
	 * @param size
	 *            the size
	 * @return the entity partition
	 */
	public Partition createPartition(String key, int size) {
		Partition partition = new Partition(registry, aspects, this, size);
		partitionMap.put(key, partition);
		return partition;
	}

	/**
	 * Gets the partition.
	 *
	 * @param key
	 *            the key
	 * @return the partition
	 */
	public Partition getPartition(String key) {
		return partitionMap.get(key);
	}

	/**
	 * Removes the partition.
	 *
	 * @param key
	 *            the key
	 * @return the entity partition
	 */
	public Partition removePartition(String key) {
		return partitionMap.remove(key);
	}

	/**
	 * Register entity.
	 *
	 * @param entity
	 *            the entity
	 * @param partition
	 *            the partition
	 */
	protected void registerEntity(Entity entity, Partition partition) {
		if (entityMap.containsKey(entity)) {
			entityMap.get(entity).add(partition);
		} else {
			Set<Partition> partitions = new HashSet<>();
			partitions.add(partition);
			entityMap.put(entity, partitions);
		}
	}

	/**
	 * Unregister entity.
	 *
	 * @param entity
	 *            the entity
	 * @param partition
	 *            the partition
	 */
	protected void unregisterEntity(Entity entity, Partition partition) {
		Set<Partition> partitions = entityMap.get(entity);
		if (partition != null) {
			partitions.remove(partitions);

			if (partitions.size() == 0) {
				entityMap.remove(entity);
			}
		}
	}

	/**
	 * Copy.
	 *
	 * @param indexSrc
	 *            the index from
	 * @param partitionSrcKey
	 *            the partition from key
	 * @param partitionTgtKey
	 *            the partition to key
	 * @throws PartitionFullException
	 *             the partition full exception
	 * @throws ComponentNotFoundException
	 */
	public void copy(int indexSrc, String partitionSrcKey, String partitionTgtKey)
			throws PartitionFullException, ComponentNotFoundException {
		Partition partitionSrc = getPartition(partitionSrcKey);
		Partition partitionTgt = getPartition(partitionTgtKey);
		Entity entity = partitionSrc.getEntity(indexSrc);

		int indexTgt = partitionTgt.addEntity(entity);

		Iterator<Class<? extends Component>> it = registry.iterator();

		while (it.hasNext()) {
			Class<? extends Component> clazz = it.next();
			Partition.copy(clazz, indexSrc, indexTgt, partitionSrc, partitionTgt);
		}
	}

	/**
	 * Move.
	 *
	 * @param indexFrom
	 *            the index from
	 * @param partitionFromKey
	 *            the partition from key
	 * @param partitionToKey
	 *            the partition to key
	 * @throws PartitionFullException
	 *             the partition full exception
	 * @throws ComponentNotFoundException
	 */
	public void move(int indexFrom, String partitionFromKey, String partitionToKey)
			throws PartitionFullException, ComponentNotFoundException {
		copy(indexFrom, partitionFromKey, partitionToKey);

		Partition partitionFrom = getPartition(partitionFromKey);
		partitionFrom.removeEntity(indexFrom);
	}

	/**
	 * Find.
	 *
	 * @param entity
	 *            the entity
	 * @return the sets the
	 */
	public Set<Partition> find(Entity entity) {
		Set<Partition> copy;
		Set<Partition> partitions = entityMap.get(entity);

		if (partitions != null) {
			copy = new HashSet<>(partitions);
		} else {
			copy = new HashSet<>();
		}
		return copy;
	}

	/**
	 * Destroy.
	 *
	 * @param entity
	 *            the entity
	 * @return true, if successful
	 */
	public boolean destroy(Entity entity) {
		Set<Partition> partitions = entityMap.get(entity);
		if (partitions != null) {
			for (Partition partition : partitions) {
				try {
					int index = partition.findEntity(entity);
					partition.removeEntity(index);
					return true;
				} catch (EntityNotFoundException e) {
					return false;
				}
			}
		}
		return false;
	}

	public static class Builder implements IDomain, IBuild {

		private final ComponentRegistry registry;
		private Aspect[] aspects;

		/**
		 * Instantiates a new builder.
		 */
		public Builder() {
			this.registry = new ComponentRegistry();
		}

		/**
		 * Register components.
		 *
		 * @param clazzez
		 *            the clazzez
		 * @return the builder
		 */
		@SafeVarargs
		public final IDomain registerComponents(Class<? extends Component>... clazzez) {
			for (Class<? extends Component> clazz : clazzez) {
				registry.add(clazz);
			}
			return this;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.blang.artie.entity.World.IDomain#registerDomains(com.blang.artie.
		 * entity.Aspect[])
		 */
		@Override
		public IBuild registerDomains(Aspect... aspects) {
			this.aspects = aspects;
			return this;
		}

		/**
		 * Builds the.
		 *
		 * @return the entity manager
		 */
		@Override
		public World build() {
			return new World(this);
		}

	}

	public interface IDomain {

		/**
		 * Register domains.
		 *
		 * @param aspects
		 *            the aspects
		 * @return the i build
		 */
		public IBuild registerDomains(Aspect... aspects);
	}

	public interface IBuild {

		/**
		 * Builds the.
		 *
		 * @return the world
		 */
		public World build();
	}
}
