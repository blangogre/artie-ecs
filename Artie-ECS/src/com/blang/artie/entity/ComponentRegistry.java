/*
 * MIT License
 * 
 * Copyright (c) 2016 Brian C. Lang
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.blang.artie.entity;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

// TODO: Auto-generated Javadoc
public class ComponentRegistry implements Iterable<Class<? extends Component>> {

	private final Set<Class<? extends Component>> registry;

	/**
	 * Instantiates a new component registry.
	 */
	protected ComponentRegistry() {
		registry = new HashSet<>();
	}

	/**
	 * Contains.
	 *
	 * @param clazz
	 *            the clazz
	 * @return true, if successful
	 */
	public boolean contains(Class<? extends Component> clazz) {
		return registry.contains(clazz);
	}

	/**
	 * Register.
	 *
	 * @param clazz
	 *            the clazz
	 * @return true, if successful
	 */
	protected boolean add(Class<? extends Component> clazz) {
		return registry.add(clazz);
	}

	/**
	 * Unregister.
	 *
	 * @param clazz
	 *            the clazz
	 * @return true, if successful
	 */
	protected boolean remove(Class<? extends Component> clazz) {
		return registry.remove(clazz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Class<? extends Component>> iterator() {
		return registry.iterator();
	}

}
